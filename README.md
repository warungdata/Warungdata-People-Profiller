# Mata-Dewa
Mata-Dewa is people finder. If target people ( actually target device ) is your nearest Mata-Dewa will alert you.

Mata-Dewa working with kismetclient.
Please visit for more: 
https://github.com/PaulMcMillan/kismetclient

# Dependencies
 - espeak

Usage:
```sh
$ ./Mata-Dewa.py -i INTERFACE
```


